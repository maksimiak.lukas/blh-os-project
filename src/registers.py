from src.Common.format import int_to_register_format
from src.type_defs import WORD_SIZE, BASE_16, REGISTER_SIZE


class StatusFlagRegister:

    def __init__(self):
        self._bytes = None
        self.__dict__['_bytes'] = {
            'SF': False,
            'CF': False,
            'OF': False,
            'ZF': False,
        }

    def get_value(self, flag):
        return self._bytes[flag]

    def set_value(self, flag, value):
        if flag in self._bytes:
            self._bytes[flag] = value


class Cell:

    def __init__(self):
        self.size = WORD_SIZE
        self.padd = '{{: >{0}}}'.format(self.size)
        self._value = '0' * self.size

    def set_value(self, value):
        value = str(value)
        if len(value) <= self.size:
            self._value = self.padd.format(value)

    def get_value(self) -> str:
        # print("get value returns: ", self._value)
        return self.padd.format(self._value)


class Register:

    # 4 byte register for PTR and SP
    # operates in hexadecimal format
    # gal būtų patogu registre saugot ir skaitinę reikšmę?
    def __init__(self):
        self._int_value = 0
        self._value = '0000'
        self.size = REGISTER_SIZE
        # self.padd = '{{: >{0}}}'.format(self.size)

    # hex value formatas : 0000 - FFFF
    def set_value(self, hex_value):
        # tikrinam ar 4 simboliai, konvertuojam i inta ir paziurim ar reiksme priklauso [0, 255^2]
        if len(hex_value) != 4:
            raise ValueError("Invalid value")

        int_value = int(hex_value, BASE_16)

        if int_value < 0 or int_value > 255 * 255:
            raise ValueError("Invalid value")

        self._value = hex_value
        self._int_value = int_value

    def get_value(self) -> str:
        # print("get value returns: ", self._value)
        return self._value.zfill(self.size - len(self._value))

    '''def int_to_register_format(self, int_value):
        if int_value < 0 or int_value > 255 * 255:
            raise ValueError("Invalid value")

        hex_value = hex(int_value)[2:]
        # '54' -> '0054'
        hex_value = hex_value.zfill(self.size)

        return hex_value'''

    def set_int_value(self, int_value):
        print("int_value: ", int_value)
        self._int_value = int_value
        formatted_value = int_to_register_format(int_value)
        print("formatted_value: ", formatted_value)
        self._value = formatted_value

    def get_int_value(self) -> int:
        return self._int_value


class BinaryRegister:

    def __init__(self):
        self.state = 0

    def get_value(self):
        return self.state

    def set_value(self, value):
        if value not in [0, 1]:
            raise ValueError("Invalid value")
        self.state = value
