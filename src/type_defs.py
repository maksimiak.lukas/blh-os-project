NUM_COLS = 256
NUM_ROWS = 756

WORD_SIZE = 8

ADDRESS_LENGTH = 6

BASE_16 = 16

BLOCK_LENGTH = 256
VM_BLOCK_COUNT = 256
# vietoje NUM_ROWS galimai, nes num_rows mįslingokai skamba
RM_BLOCK_COUNT = 3 * VM_BLOCK_COUNT

STACK_BLOCK_COUNT = int(VM_BLOCK_COUNT / 2)
STACK_OFFSET = VM_BLOCK_COUNT - STACK_BLOCK_COUNT

SUPERVISOR = 0
USER = 1

MAX_VM_COUNT = 2  # ????

SF_FLAG = 0
CF_FLAG = 1
OF_FLAG = 2
ZF_FLAG = 3

REGISTER_SIZE = 4

# data sources used by channel device
# vartotojo atmintis
VIRTUAL_MEMORY = 0
# supervizorinė (ir vartotojo) atmintis
REAL_MEMORY = 1
OUTER_MEMORY = 2
INPUT = 3
OUTPUT = 4
STACK = 5
# data sources used by channel device

# komandu masyvas
COMMANDS = ["PSH", "ADD", "SUB", "CMP", "POP", "INP", "PRT", "NOT", "OR", "AND"]

HARD_DRIVE_CAPACITY = 4096

