from src.type_defs import ADDRESS_LENGTH, BASE_16


class Pager:

    def __init__(self, real_machine):
        # self._ptr_register = ptr_register  # argumentas PTR registras sukurtas VM konstruktoriuje
        # (šiuo metu - RealMachine.load_virtual_machine() funkcijoje)
        # o jos reikia? aš galvojau, kad mes darysim atmintį tik pačioj RM,
        # visi kiti tik operuos adresais ir kreipsis į RM
        # self._memory = memory  # realios masinos atmintis
        self.real_machine = real_machine

    # input  -> virtualus adresas 16-taineje sistemoje
    # output -> realus adresas 16-taineje sistemoje
    def get_real_address(self, virtual_address_pgr_format, ptr_value):
        # gauname adresa x1x2x3x4 formatu (16-taineje sistemoje)
        # max adresas FFFF = 65535, VM atmintis 256*256 = 65536 -> I(0, 65535)
        # perdirbame adresa, kad gautume sias reiksmes atskirai

        if len(virtual_address_pgr_format) != ADDRESS_LENGTH:
            raise ValueError("Invalid address format (0x....)")

        # [0] ir [1] yra 0x
        x1 = int(virtual_address_pgr_format[2], BASE_16)
        x2 = int(virtual_address_pgr_format[3], BASE_16)
        x3 = int(virtual_address_pgr_format[4], BASE_16)
        x4 = int(virtual_address_pgr_format[5], BASE_16)

        # a0 is not used
        a1 = int(ptr_value[1], BASE_16)
        a2 = int(ptr_value[2], BASE_16)
        a3 = int(ptr_value[3], BASE_16)


        # gauname reiksme atminties vietos: [256 * (100a1 + 10a2 + a3) + (x1 * 16 + x2)]

        location = 256 * (100 * a1 + 10 * a2 + a3) + (x1 * 16 + x2)
        cell_value = int(self.real_machine.real_memory.get_cell_value(location))

        # galiausiai skaiciuojame realu adresa
        real_address = hex(256 * cell_value + (x3 * 16 + x4))

        # reikia dar patikrinti ar gerai veikia
        return real_address

    # patogumo dėlei
    def get_real_address_int(self, virtual_address_pgr_format, ptr_value):
        real_address_hex = self.get_real_address(virtual_address_pgr_format, ptr_value)
        return int(real_address_hex, BASE_16)




