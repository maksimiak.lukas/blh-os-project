import os

from src.Common.format import int_to_pager_format
from src.type_defs import *


# reikėtų turėt visas atmintis, į realią atmintį įeina ir naudotojo, ir supervizorinė atmintys
# supervizorinės atminties adresas yra adresas realioje atmintyje, jo nereikia apdoroti
# diskas - kaip adresuojame diską? eilutėmis, štai kaip
# naudotojo atmintis - paduodame virtualų adresą, naudojame pager'į konversijai
class Channel:

    hdd_path = os.path.join(os.path.dirname(__file__), 'machine', 'real', 'hard.txt')

    def __init__(self, pager, real_memory, stack, hard_disk=None, input=None, output=None):
        # is realios masinos galime pasiekti atminti, bet gal užtenka pačios atminties?
        self._data_sources = []
        # ir user memory, ir super memory, yra real memory
        self._data_sources.append(None)
        self._data_sources.append(real_memory)
        self._data_sources.append(hard_disk)
        self._data_sources.append(input)
        self._data_sources.append(output)
        self._data_sources.append(stack)
        self._pager = pager


    def write_data(self, source, destination, source_address, dest_address, length, ptr_value=''):
        s_address = source_address
        d_address = dest_address
        # jei source arba dest yra VIRTUAL_MEMORY,
        # konvertuojame virtualų adresą į realų ir pakeičiame source į REAL_MEMORY
        if source == VIRTUAL_MEMORY:
            if ptr_value == '':
                raise ValueError("ptr value not passed!")
            # konvertuojame
            s_address_pgr_format = int_to_pager_format(s_address)
            s_address = self._pager.get_real_address_int(s_address_pgr_format, ptr_value)
            source = REAL_MEMORY

        if destination == VIRTUAL_MEMORY:
            if ptr_value == '':
                raise ValueError("ptr value not passed!")
            # konvertuojame
            d_address_pgr_format = int_to_pager_format(d_address)
            d_address = self._pager.get_real_address_int(d_address_pgr_format, ptr_value)
            destination = REAL_MEMORY

        hdd_lines = []
        if source == OUTER_MEMORY or destination == OUTER_MEMORY:
            # open hard drive file for reading
            hdd_file = open(self.hdd_path, "r")
            hdd_lines = hdd_file.readlines()
            # print(f.read())
            # go to specified line in the file

        # print("lines:")
        for lineIdx in range(0, HARD_DRIVE_CAPACITY - len(hdd_lines)):
            hdd_lines.append('\n')

        print("lines:")
        for line in hdd_lines:
            if line.strip() != '':
                print(line)

        for valueIdx in range(0, length):
            # get source value
            if source == REAL_MEMORY:
                s_value = self._data_sources[REAL_MEMORY].get_cell_value(s_address)
            elif source == OUTER_MEMORY:
                # read from the specified line
                s_value = hdd_lines[s_address]
            # TODO input, output

            # write value to destination
            if destination == REAL_MEMORY:
                self._data_sources[REAL_MEMORY].put_data(d_address, s_value)
            elif destination == OUTER_MEMORY:
                hdd_lines[d_address] = s_value + '\n'
            # TODO input, output

            s_address += 1
            d_address += 1

        if source == OUTER_MEMORY:
            hdd_file.close()
        # save hard drive if it has been written to
        if destination == OUTER_MEMORY:
            # open hard drive file for writing
            project_dir = os.path.dirname(__file__)
            hdd_file = open(self.hdd_path, "w")
            # write to hard drive file
            for line in hdd_lines:
                hdd_file.write(line)

            hdd_file.close()

    # stack, vmem, rmem, disk
    # def stack_to_vmem(self, s_address, vm_address):
