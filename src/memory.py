from src.type_defs import *
from src.registers import Cell


class Block:

    def __init__(self, vm_id=-1):
        self.vm = vm_id
        self.cells = [Cell() for _ in range(BLOCK_LENGTH)]

    def assign_to_vm(self, vm_id):
        self.vm = vm_id

    def get_vm(self):
        return self.vm


# should probably be accessed through RM
class RealMemory:

    def __init__(self):
        # real memory - a list of blocks
        self.memory = [Block() for _ in range(RM_BLOCK_COUNT)]

    def get_address_tuple(self, address_int):
        block_idx = int(address_int / BLOCK_LENGTH)
        cell_idx = int(address_int % BLOCK_LENGTH)
        return block_idx, cell_idx

    def get_cell_value(self, address_int):
        block_idx, cell_idx = self.get_address_tuple(address_int)
        return self.memory[block_idx].cells[cell_idx].get_value()

    def put_data(self, address_int, value):
        block_idx, cell_idx = self.get_address_tuple(address_int)
        self.memory[block_idx].cells[cell_idx].set_value(value)


# testas
'''
real_memory = RealMemory()
ptr_reg = Register()
ptr_reg.set_value(0x1234)
my_pager = Pager(ptr_reg)
my_pager.get_real_address("0xA79F")
print(my_pager.get_real_address("0xA79F"))
'''

# testo pabaiga
