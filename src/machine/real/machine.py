from src.machine.virtual.machine import VirtualMachine
from src.memory import RealMemory
from src.paging import Pager
from src.registers import Register
from src.type_defs import *


class RealMachine:
    def __init__(self):
        # initially let the RM run in supervisor mode
        self.mode = SUPERVISOR
        # VM list
        self.virtual_machines = []
        self.real_memory = RealMemory()
        self.pager = Pager(self)

    def set_mode(self, mode):
        self.mode = mode

    def load_virtual_machine(self) -> VirtualMachine:
        # VMs paging list is saved in the last available block of memory
        # galima ir kąąąąąą kitą parinkt, nežinau kur geriausia būtų saugot puslapiavimo lenteles
        # assign blocks for VM from the block list
        # currently we just assign first unused blocks found
        # some blocks should probably be marked as non - usable
        # for now the last 256 blocks are just left out

        # indeksas realioje atmintyje
        idx = 0
        # indeksas virtualioje atmintyje
        block_count = 0
        paging_list_address = -1
        while (idx < MAX_VM_COUNT * VM_BLOCK_COUNT) and (block_count < VM_BLOCK_COUNT):
            if self.real_memory.memory[idx].vm < 0:
                block_count += 1
                # assign the block to the VM under creation
                self.real_memory.memory[idx].vm = len(self.virtual_machines)
                # jei tai pirmas rastas blokas, naudokime jį puslapių lentelei:
                if block_count == 1:
                    paging_list_address = idx
                    continue

                self.real_memory.memory[paging_list_address].cells[block_count - 2].set_value(idx)
            idx += 1

        # create ptr register for this VM and assign paging_list_address as its value

        ptr_reg = Register()
        # int_value = int(hex(paging_list_address), BASE_16)
        # print("int value: ", paging_list_add
        # return new VM, pass its PTR and this RM for construction
        self.virtual_machines.append(VirtualMachine(paging_list_address, self, self.pager))
        return self.virtual_machines[len(self.virtual_machines) - 1]

    # gauna memory, kuriame yra pakrauta programa
    # memory_address - nuo kur prasideda uzkrauta programa
    # length - kiek celiu programa uzima
    # jeigu validacija sekminga - grazina 1
    def validate_input_program(self, memory_address, length):

        # programa turetu prasideti $(AMJ)
        start = (self.real_memory.get_cell_value(memory_address)).strip()

        if start != '$AMJ':
            raise ValueError("Invalid value: program should start with '$AMJ'")

        max_output_length = (self.real_memory.get_cell_value(memory_address + 1)).strip()
        if not max_output_length.isnumeric():
            raise ValueError("Invalid value: max output length should be numeric")

        program_name = (self.real_memory.get_cell_value(memory_address + 2)).strip()

        begin = memory_address + 3
        end = memory_address + 3 + length - 1 # -1 $END simboliui

        for index in range(begin, end):

            command = self.real_memory.get_cell_value(index).strip()
            if command[0] == '$':
                # jeigu komanda prasideda siuo simboliu
                # po simbolio gali eiti vien skaiciai
                if not command[1:].isnumeric():
                    raise ValueError("Invalid value: numbers expected after '$' symbol")
            elif command.isalnum():
            # jeigu komanda nesusideda vien is skaiciu
            # pirmi 3 arba 2 (OR atveju) char'ai turetu buti is komandu saraso
                if (command[0:2] or command[0:3]) not in COMMANDS:
                    raise ValueError("Invalid value: incorrect command")

        # galiausiai patikriname ar programa baigiasi $END
        finish = self.real_memory.get_cell_value(end + 1).strip()
        if finish != "$END":
            raise ValueError("Invalid value: $END not found")

        return 1

    # test
    def print_r_memory(self, print_empty=False):
        print("real memory:")
        block_idx = -1
        for block in self.real_memory.memory:
            block_idx += 1
            cell_idx = -1
            for cell in block.cells:
                cell_idx += 1
                if print_empty or int(cell.get_value()) > 0:
                    print("[{0}][{1}]:{2}".format(block_idx, cell_idx, cell.get_value()))
        print("*************************************************************************")
