from src.processor import Processor
from src.type_defs import *


class Stack:
    # i beg you Samir implement stack
    # fix stack to operate through reference to RMs memory instead of its own !!

    def __init__(self, VM):
        self.memory = None
        print("IAM STACK")
        # stack size in cells
        self.memory_limit = STACK_BLOCK_COUNT * BLOCK_LENGTH
        # inicializuojant SP reikšmė = maxvalue, kad pirmas push() įrašytų į memory[0][0]
        self.VM = VM
        self.SP = VM.SP
        self.SP.set_int_value(self.memory_limit - 1)
        self.pager = VM.pager
        self.ptr = VM.ptr

    def pop(self):
        old_sp_value = self.SP.get_int_value()
        new_sp_value = (old_sp_value - 1) % self.memory_limit
        self.SP.set_value(int(new_sp_value))

        block_idx = int(old_sp_value / BLOCK_LENGTH)
        cell_idx = int(old_sp_value % BLOCK_LENGTH)

        # VM stack starts at [128][0]
        return self.memory[STACK_OFFSET + block_idx][cell_idx].get_value()

    def push(self, value):
        new_sp_value = (self.SP.get_int_value() + 1) % self.memory_limit
        self.SP.set_value(new_sp_value)
        block_idx = int(new_sp_value / BLOCK_LENGTH)
        cell_idx = int(new_sp_value % BLOCK_LENGTH)

        # VM stack starts at [128][0]
        self.memory[STACK_OFFSET + block_idx][cell_idx].set_value(value)

    # adresas yra 4 skaitmenų hex skaičius 0000 - FFFF
    def push_from_memory(self, address):
        value = self.VM.get_value(address)
        self.push(value)

    def pop_to_memory(self, address):
        value = self.pop()
        # write value to memory
        self.VM.set_value(address, value)

    # testavimo tikslais pridėta funkcijėlė
    def print_stack(self, print_empty=False):
        print("stack:")
        block_idx = -1
        for block in self.memory:
            block_idx += 1
            cell_idx = -1
            for cell in block:
                cell_idx += 1
                if print_empty or int(cell.get_value()) > 0:
                    print("[{0}][{1}]:{2}".format(block_idx - STACK_OFFSET, cell_idx, cell.get_value()))
