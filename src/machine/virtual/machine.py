from src.machine.virtual.stack import Stack
from src.paging import Pager
from src.registers import StatusFlagRegister, Register
from src.type_defs import *


class VirtualMachine:

    def __init__(self, ptr_value, real_machine, pager):
        self.memory = None
        self.real_machine = real_machine
        self.SF = StatusFlagRegister()  # status flag register
        self.SP = Register()
        self.ptr = Register()
        self.ptr.set_int_value(ptr_value)
        self.pager = pager
        self._stack = Stack(self)

    # gets value stored in real memory, uses pager to get real address
    def get_value(self, v_address):
        real_address_int = self.pager.get_real_address_int(v_address, self.ptr.get_value())
        return self.real_machine.real_memory.get_cell_value(real_address_int)

    # sets value in real memory, uses pager to get real address
    def set_value(self, v_address, value):
        real_address_int = self.pager.get_real_address_int(v_address, self.ptr.get_value())
        self.real_machine.real_memory.put_data(real_address_int, value)

    # interprets the program and executes it
    def execute_program(self):
        # pirmos atminties ląstelės reikšmė:
        # self.get_value(0)
        index = 0
        place_in_memory = 0

        while True:
            # imame lasteles reiksme
            value = self.get_value(index)

            # padidiname indeksa, kad kita karta imtume jau kita lastele
            # apkarpome, kad nebutu leading zeros
            index = index + 1
            value = value.trim()

            # patikriname kas tai per reiksme
            if value == "$END":
                # stabdome vykdyma
                return
            if value == "$AMJ":
                # tai programos pradzia
                continue
            if value[0] == '$':
                # prasideda zyme i kuria vieta rasysime reiksmes
                place_in_memory = value[1:]
                continue
            if value.isnumeric():
                # reiksmes kurias idedame
                self.set_value(place_in_memory, value)
                place_in_memory = place_in_memory + 1
                continue
            if value.isalnum():
                if len(value) == 2:
                    # tai yra OR komanda
                    self.cmd_or()
                    continue
                elif len(value) == 3:
                    # tai gali buti: AND, ADD, SUB, CMP, PRT, INP
                    if value == "ADD":
                        self.cmd_add()
                    if value == "AND":
                        self.cmd_and()
                    if value == "SUB":
                        self.cmd_sub()
                    if value == "CMP":
                        self.cmd_cmp()
                    # TODO: PRT ir INP komandos
                    # if value == "INP":
                        # set input interrupt flag
                    # if value == "PRT":
                        # set output interrupt flag

                    continue
                elif len(value) > 3:
                    # tai gali buti: PSHxxxx, POPxxxx, JLSwxyz
                    # tai gali buti: JGRwxyz, JEQwxyz, JNEwxyz
                    command_type = value[0:2]
                    address = value[3:6]
                    if command_type == "PSH":
                        self.cmd_push(address)
                    if command_type == "POP":
                        self.cmd_pop(address)
                    if command_type == "JLS":
                        self.cmd_cmp()
                        if self.SF.get_value(SF_FLAG) == '1' and self.SF.get_value(ZF_FLAG) == '0':
                            index = address
                    if command_type == "JEQ":
                        self.cmd_cmp()
                        if self.SF.get_value(ZF_FLAG) == '1':
                            index = address
                    if command_type == "JNE":
                        self.cmd_cmp()
                        if self.SF.get_value(ZF_FLAG) == '0':
                            index = address
                    if command_type == "JGR":
                        self.cmd_cmp()
                        if self.SF.get_value(SF_FLAG) == '0' and self.SF.get_value(ZF_FLAG) == '0':
                            index = address
                    continue

        return 0

    # testavimo tikslais pridėtos funkcijėlės
    # currently only for testing
    def int_to_register_format(self, int_value):
        if int_value < 0 or int_value > 255 * 255:
            raise ValueError("Invalid value")

        hex_value = hex(int_value)[2:]
        # '54' -> '0054'
        hex_value = hex_value.zfill(4)

        return hex_value

    def int_to_pager_format_address(self, virtual_address_int, ptr_value):
        hex_address = self.int_to_register_format(virtual_address_int)
        complete_address = '0x' + hex_address
        return complete_address

    def print_v_memory(self, print_empty=False):
        print("virtual memory:")
        for address in range(0, (BLOCK_LENGTH - 1) * (VM_BLOCK_COUNT - 1)):
            pgr_address = self.int_to_pager_format_address(address, self.ptr.get_value())
            value = self.get_value(pgr_address)
            if int(value) == 0:
                continue

            block_idx = int(address / BLOCK_LENGTH)
            cell_idx = address % BLOCK_LENGTH
            print("[{0}][{1}]:{2}".format(block_idx, cell_idx, value))

    # arithmetic commands
    def cmd_add(self):
        op1 = self._stack.pop()
        op2 = self._stack.pop()
        result = int(op1) + int(op2)
        self._stack.push(result)

    def cmd_sub(self):
        op1 = self._stack.pop()
        op2 = self._stack.pop()

        if int(op1) > int(op2):
            self.SF.set_value(SF_FLAG, 1)
        else:
            self.SF.set_value(SF_FLAG, 0)

        result = int(op1) - int(op2)

        self._stack.push(result)

    def cmd_mul(self):
        op1 = self._stack.pop()
        op2 = self._stack.pop()
        result = int(op1) * int(op2)

        hex_value = hex(result)[2:]
        if len(hex_value) > 8:
            self.SF.set_value(OF_FLAG, 1)
        else:
            self.SF.set_value(OF_FLAG, 0)
        self._stack.push(result)

    def cmd_div(self):
        op1 = self._stack.pop()
        op2 = self._stack.pop()
        result = int(op2) / int(op1)
        self._stack.push(result)

    # logical commands
    def cmd_and(self):
        op1 = self._stack.pop()
        op2 = self._stack.pop()

        op1_int = int(op1)
        op2_int = int(op2)

        result = op1_int & op2_int

        self.stack.push(result)

    def cmd_or(self):
        op1 = self._stack.pop()
        op2 = self._stack.pop()

        op1_int = int(op1)
        op2_int = int(op2)

        result = op1_int | op2_int

        self.stack.push(result)

    def cmd_not(self):
        op1 = self._stack.pop()

        op1_int = int(op1)

        result = ~op1_int

        self.stack.push(result)

    def cmd_push(self, address):
        self._stack.push_from_memory(address)

    def cmd_pop(self, address):
        self._stack.pop_to_memory(address)

        # comparison
        '''
        Jei ST[SP] > ST[SP-1], Sign Flag (0) = 0, Zero flag (3) = 0, Jei ST[SP] < ST[SP-1],
         Sign Flag = 1, Zero flag = 0, Jei ST[SP] = ST[SP-1], Sign Flag = 0, Zero flag = 1. 
        '''

    def cmd_cmp(self):
        op1 = self._stack.pop()
        op2 = self._stack.pop()

        op1_int = int(op1)
        op2_int = int(op2)

        if op1_int > op2_int:
            self.SF.set_value(SF_FLAG, 0)
            self.SF.set_value(ZF_FLAG, 0)
        elif op1_int < op2_int:
            self.SF.set_value(SF_FLAG, 1)
            self.SF.set_value(ZF_FLAG, 0)
        else:
            self.SF.set_value(SF_FLAG, 0)
            self.SF.set_value(ZF_FLAG, 1)

        self._stack.push(op2)
        self._stack.push(op1)

    @property
    def stack(self):
        return self._stack


'''
class Interpreter:
    def __init__ (self, stack):
        self._stack = stack

    def read_next_command(self):

'''
