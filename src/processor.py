from src.registers import BinaryRegister, Register, StatusFlagRegister


class Processor:
    # per VM
    SF = StatusFlagRegister()  # status flag register
    SP = Register()  # stack pointer register

    PI = BinaryRegister()  # program interupt register
    SI = BinaryRegister()  # supervisor interupt register
    TI = BinaryRegister()  # timer interupt register

    IC = Register()  # instruction count register
    MODE = BinaryRegister()  # user or supervisor register
    PTR = Register()  # register

    # def __init__(self):