import enum

from src.machine.real.machine import RealMachine
from src.registers import Register, StatusFlagRegister


class ProcessState(enum.Enum):
    s_run = 0
    s_ready = 1
    s_block = 2
    s_ready_stop = 3
    s_block_stop = 4


class ProcessList:

    def __init__(self, f_kernel, f_processes):
        self.f_kernel = f_kernel
        self.f_processes = f_processes


class Process:

    def __init__(self, f_id, f_list: ProcessList, f_saved_registers, f_processor, f_owned_res, f_state: ProcessState,
                 f_priority,
                 f_parent,
                 f_children, f_kernel, f_user_name, f_stack, f_stack_size):
        self.f_id = f_id
        self.f_list = f_list
        self.f_saved_registers = f_saved_registers
        self.f_processor = f_processor
        self.f_created_res = []
        self.f_owned_res = f_owned_res
        self.f_state = f_state
        self.f_priority = f_priority
        self.f_parent = f_parent
        self.f_children = f_children
        self.f_kernel = f_kernel
        self.f_user_name = f_user_name
        self.f_stack = f_stack
        self.f_stack_size = f_stack_size

    def run(self):
        self.f_priority -= 1

        # iskiriami resursai
        pass

    def is_blocked(self):
        return self.f_state == ProcessState.s_block

    def block(self):
        self.f_state = ProcessState.s_block

    def activate(self):
        self.f_state = ProcessState.s_ready


class TPIReg(enum.Enum):
    pi_none = 0
    pi_invalid_op_code = 1
    pi_invalid_address = 2
    pi_overflow = 3
    pi_illegal_assignment = 4


class TSIReg(enum.Enum):
    si_none = 0
    si_get_data = 1
    si_put_data = 2
    si_end = 3


class SavedRegisters:
    def __init__(self, SP: Register, PTR: Register, SF: StatusFlagRegister, IC: Register, PI: TPIReg, SI: TSIReg):
        self.SP = SP
        self.IC = IC
        self.SF = SF
        self.PTR = PTR
        self.PI = PI
        self.SI = SI


class TKernel:
    def __init__(self, f_processes: ProcessList, f_resources, f_ready_proc: ProcessList, f_run_proc: ProcessList,
                 f_real_machine: RealMachine):
        self.f_processes = f_processes
        self.f_resources = f_resources
        self.f_ready_proc = f_ready_proc
        self.f_run_proc = f_run_proc
        self.f_real_machine = f_real_machine
