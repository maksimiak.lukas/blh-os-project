from src.process.process import Process


class ProcessManager:
    current_process: Process = None
    process_queue = None

    def __init__(self, current_process):
        self.current_process = self.current_process
        self.process_queue = current_process.f_list.f_processes

    def find_process_index_by_id(self, process: Process):
        return next((i for i, item in enumerate(self.process_queue) if item.id == process.f_id), -1)

    def plan(self):

        if not self.current_process:
            # Start main process?
            print("No current process!")
            return

        if not self.current_process.is_blocked():
            self.process_queue.append(self.current_process)

        if len(self.process_queue) == 0:
            # sistemoje visalaik turetu but bent vienas zombinis procesas su zemiausiu prioritetu
            print("FATAL ERROR! No active processes")
            return

        self.process_queue.sort(key=lambda p: p.f_priority, reverse=True)

        for process in self.process_queue:
            if not process.is_blocked():
                self.current_process = process
                process.run()
                break

    def create_process(self):
        pass

    def kill_process(self):
        pass

    def block_process(self, process: Process):
        if not process.is_blocked():
            process.block()

            # remove from process queue

            index = self.find_process_index_by_id(process)

            if index != -1:
                self.process_queue.pop(index)

    def activate_process(self, process: Process):

        if process.is_blocked():
            process.activate()

            # add process to process queue

            self.process_queue.append(process)
