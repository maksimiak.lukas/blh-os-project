from src.type_defs import REGISTER_SIZE


def int_to_register_format(int_value):
    if int_value < 0 or int_value > 255 * 255:
        raise ValueError("Invalid value")

    hex_value = hex(int_value)[2:]
    # '54' -> '0054'
    hex_value = hex_value.zfill(REGISTER_SIZE)

    return hex_value


def int_to_pager_format(int_value):
    reg_format = int_to_register_format(int_value)
    return '0x' + reg_format
