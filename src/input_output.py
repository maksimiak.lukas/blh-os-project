class InputDevice:
    # writing input to stack should be done via channel device
    def get_input(self):
        inp = input(">")
        return inp


class OutputDevice:
    def __init__(self):
        self.buffer = []

    def add_to_buffer(self, output_value):
        self.buffer.append(output_value)

    def print_output(self, value):
        if len(self.buffer) == 0:
            print("output buffer is empty")
            return

        if idx < 0:
            idx = len(self.buffer - 1)

        value = self.buffer.pop(idx)
        print(value)