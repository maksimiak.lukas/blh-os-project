from src.channel import Channel
from src.machine.real.machine import RealMachine
from src.type_defs import *

# TODO:
# puslapiavimas
# atminties paskirstymas VM
# apibrezti STACK'a virtualioj masinoj
# apibrezti visas komandas ir ka jos daro

# VM initialization test
RM = RealMachine()
# RM.print_r_memory()
VM = RM.load_virtual_machine() # <- pasisetina ptr registras
# RM.print_r_memory()

# test end

# testas


virtual_address = "0x1234"
print("virtual address: {0}".format(virtual_address))
real_address = VM.pager.get_real_address(virtual_address, VM.ptr.get_value())
print("real address:", real_address)
VM.set_value(virtual_address, 0x1234)
print("Setting value:", 0x1234)
real_address_int = int(real_address, BASE_16)
length = 10

for cellIdx in range(0, length):
    RM.real_memory.put_data(real_address_int + cellIdx, 0x1234)

set_val = RM.real_memory.get_cell_value(real_address_int)
RM.real_memory.put_data(real_address_int + 1, 0x1234)
print("value[{0}]:{1}".format(real_address, set_val))
#RM.print_r_memory()
'''
RealMachine.real_memory.put_data()
ptr_reg = Register()
ptr_reg.set_value(0x1234)
my_pager = Pager(ptr_reg)
my_pager.get_real_address("0xA79F")
print(my_pager.get_real_address("0xA79F"))
'''
# testo pabaiga

# channel device testas
VM.print_v_memory()
v_address_int = 5555
print ("rašome {0} virtualiu adresu {1}".format(set_val, v_address_int))
# (self, pager, real_memory, stack, hard_disk=none, input=none, output=none):
channel_device = Channel(VM.pager, RM.real_memory, VM.stack)
channel_device.write_data(REAL_MEMORY, VIRTUAL_MEMORY, real_address_int, v_address_int, length, VM.ptr.get_value())
VM.print_v_memory()
real_address_int = 47123
print ("rašome iš virtualaus adreso {0} į realų adresą {1}".format(v_address_int, real_address_int))
channel_device.write_data(VIRTUAL_MEMORY, REAL_MEMORY, v_address_int, real_address_int, length, VM.ptr.get_value())
RM.print_r_memory()

# write to hdd:
channel_device.write_data(VIRTUAL_MEMORY, OUTER_MEMORY, v_address_int, 0, length, VM.ptr.get_value())

# channel device testas
# idedam i kuria nors atminties vieta stringa - "$(AMJ)"
'''
RM.real_memory.put_data(1234, "$AMJ")
RM.real_memory.put_data(1235, "1000")
RM.real_memory.put_data(1236, "AmzinasC")
RM.validate_input_program(1234, 1)
'''

# validatoriaus testas





# validatoriaus testas
